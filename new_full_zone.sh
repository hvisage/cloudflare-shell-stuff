#!/bin/bash
#$1 the file from the user_Extract
#$2 the zone
curl https://api.cloudflare.com/host-gw.html \
  -d 'act=full_zone_set' \
  -d 'host_key='`cat host-key.api` \
-d 'user_key='`jq -r '.response.user_key' $1` \
-d 'zone_name='$2
