#!/bin/bash
set -x
UEmail=`jq -r .cloudflare_email < users/$1.json`
UAPI=`jq -r .user_api_key < users/$1.json`
curl -X GET "https://api.cloudflare.com/client/v4/zones/$2/dns_records/export" \
     -H "X-Auth-Email: $UEmail" \
     -H "X-Auth-Key: $UAPI" \
     -H "Content-Type: application/json" >> ${2}.delete_export.zone

curl -X DELETE "https://api.cloudflare.com/client/v4/zones/$2" \
     -H "X-Auth-Email: $UEmail" \
     -H "X-Auth-Key: $UAPI" \
     -H "Content-Type: application/json" | tee ${2}.delete

