#!/bin/bash
host_api_key=`cat cf-partner/host.api`
userkey=`jq '.user_key' <users/$1.json`
echo curl -s https://api.cloudflare.com/host-gw.html \
  -d 'act=full_zone_set' \
  -d 'host_key='$host_api_key \
-d "user_key=$userkey" \
-d "zone_name=$2"|sh -x
