#!/bin/bash
set -x
UEmail=`jq -r .cloudflare_email < users/$1.json`
UAPI=`jq -r .user_api_key < users/$1.json`
#curl -X GET "https://api.cloudflare.com/client/v4/zones/$2/dns_records/export" \
#     -H "X-Auth-Email: $UEmail" \
#     -H "X-Auth-Key: $UAPI" \
#     -H "Content-Type: application/json"
curl -X POST "https://api.cloudflare.com/client/v4/zones/$2/dns_records/import" \
     -H "X-Auth-Email: $UEmail" \
     -H "X-Auth-Key: $UAPI" \
     --form "file=@$3;proxied=false"
