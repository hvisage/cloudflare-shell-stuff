#!/bin/bash

curl https://api.cloudflare.com/host-gw.html \
  -d 'act=user_lookup' \
  -d "host_key=`cat cf-partner/host.api`" \
  -d 'cloudflare_email='$1  | jq .
