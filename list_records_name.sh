#!/bin/bash

ZoneId=`./fetch_zone_id.sh $1 $2`
eval `cat $1 | jq -r 'keys[] as $k | "\($k)=\"\(.[$k])\"" ' `
curl -s -X GET "https://api.cloudflare.com/client/v4/zones/"$ZoneId"/dns_records?per_page=1000" \
 -H "X-Auth-Email: $cloudflare_email" -H "X-Auth-Key: $user_api_key" \
 -H "Content-Type:  application/json"
