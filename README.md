# README #


### What is this repository for? ###

* Quick & Dirst Shell scripts to access CloudFlare APIs via curl
* typically for a Hosting partner. 

### How do I get set up? ###

- Install `curl` and `jq` as `bash` is assumed :D
- put you Host API key in `cf-partner/host.api`
- use `user_lookup_extract.sh` to create the various use api files that will be used as `$1` argument for the other scripts
- If you aren't a hosting partner, I'll need to find out how to get the same values.... I don;t yet know :D

### Contribution guidelines ###

* let me know :)
* It's shell/curl based, would be nice to have some more and cleanups

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact