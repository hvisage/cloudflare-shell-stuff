#!/bin/bash
set -x
zone_id=`./fetch_zone_id.sh $1 $2`
UEmail=`jq -r .cloudflare_email < $1`
UAPI=`jq -r .user_api_key < $1`
curl -X GET "https://api.cloudflare.com/client/v4/zones/$zone_id/dns_records/export" \
     -H "X-Auth-Email: $UEmail" \
     -H "X-Auth-Key: $UAPI" \
     -H "Content-Type: application/json"
