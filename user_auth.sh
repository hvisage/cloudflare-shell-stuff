#!/bin/bash

curl https://api.cloudflare.com/host-gw.html \
  -d 'act=user_auth' \
  -d "host_key=`cat cf-partner/host.api`" \
  -d 'cloudflare_email='$1 \
  -d 'cloudflare_pass='$2 \
  -d 'unique_id='$3
