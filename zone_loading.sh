#!/bin/bash
set -x

curl -H 'x-auth-email: '$cloudflare_email -H 'x-auth-key: '$user_api_key 'https://api.cloudflare.com/client/v4/zones/'$1'/dns_records/import' -F 'file=@'$2 | jq .| tee -a $1.loaded_status

curl -H 'x-auth-email: '$cloudflare_email -H 'x-auth-key: '$user_api_key 'https://api.cloudflare.com/client/v4/zones/'$1'/dns_records' |jq '.result[]|{type,name,content,proxied}'| tee -a $1.loaded_results

curl -H 'x-auth-email: '$cloudflare_email -H 'x-auth-key: '$user_api_key 'https://api.cloudflare.com/client/v4/zones/'$1'/dns_records/export' |jq '.result[]|{type,name,content,proxied}'| tee -a $1.loaded_export
